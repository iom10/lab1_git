<?php


// VERSION 0.2
class Calculator
{
	public function __construct()
	{
		
	}

	public function add(float $a, float $b): float
	{
		return $a + $b;
	}

	public function sub(float $a, float $b): float
	{
		return $a - $b;
	}

}

